const express = require('express')

const app = express()
const port = process.env.PORT || 8080

app.use(express.static('public'))
app.use(express.json())


app.get('/hello', (req, res, next) => {

  res.send({
    message:"🖖Live long and prosper!"
  })
})

app.listen(port, () => console.log(`🌍 listening on port ${port}`))

