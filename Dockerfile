FROM node:12.7.0-alpine

COPY . .
RUN npm install

RUN touch /tmp/.lock

CMD ["npm", "start"]